"use strict";
function isCheckNumber(number) {
    while(parseInt(number)!=number) {
        number = prompt('ERROR! Enter your number:', number);
    }  
    return +number;
}
function Fibonachi(Fib0, Fib1, serialNumber) {
   if(serialNumber>=0) {
       if (serialNumber==1) return Fib0;
       else if (serialNumber==2) return Fib1;
       else return Fibonachi(Fib0, Fib1, serialNumber - 1) + Fibonachi(Fib0, Fib1, serialNumber - 2);
   }
   else {
    if (serialNumber==-1) return Fib0;
    else if (serialNumber==-2) return Fib1;
    else return Fibonachi(Fib0, Fib1, serialNumber + 1) + Fibonachi(Fib0, Fib1, serialNumber + 2);
   }
}
let getNumberF0 = prompt('Enter first number:');
let getNumberF1 = prompt('Enter second number:');
let getSerialNumber = prompt('Enter serial number:');
getNumberF0 = isCheckNumber(getNumberF0);
getNumberF1 = isCheckNumber(getNumberF1);
getSerialNumber = isCheckNumber(getSerialNumber);
let outputFibonachi;
outputFibonachi = Fibonachi(getNumberF0, getNumberF1, getSerialNumber);
alert(outputFibonachi);